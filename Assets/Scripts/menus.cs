﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menus : MonoBehaviour {

	public GameObject menu;
	public GameObject creditos;
	public GameObject playerselect;
	public GameObject opcoes;
	public GameObject comojogar;

	void Start () {

		menu.SetActive (true);
		creditos.SetActive (false);
		playerselect.SetActive (false);
		opcoes.SetActive (false);
		comojogar.SetActive (false);

	}


	public void abrecreditos(){
		menu.SetActive (false);
		creditos.SetActive (true);
	}

	public void abreopcoes(){
		menu.SetActive (false);
		opcoes.SetActive (true);
	}

	public void abrecomojogar(){
		menu.SetActive (false);
		comojogar.SetActive (true);
	}

	public void abreplayerselect(){
		menu.SetActive (false);
		playerselect.SetActive (true);
	}

	public void voltar(){
		menu.SetActive (true);
		creditos.SetActive (false);
		playerselect.SetActive (false);
		opcoes.SetActive (false);
		comojogar.SetActive (false);
	}

	public void IrPara(string name){

		SceneManager.LoadScene (name);
	}

}
