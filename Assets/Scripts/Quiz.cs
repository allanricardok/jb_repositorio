﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quiz : MonoBehaviour
{
    public GameObject wrongAnswer;
    public GameObject rightAnswer;

    public Text rightAnswerText;
    public Text questionText;
    
    public PlayerScript player;
    
    bool answer;
    
    public void Question(){
        switch(Random.Range(1,6)){
            case 1:
                questionText.text = "É permitido passeio com animais de estimação dentro do Jardim Botânico.";
                answer = false;
                break;
            case 2:
                questionText.text = "É proibido dar comida aos animais silvestres.";
                answer = true;
                break;
            case 3:
                questionText.text = "O Jardim Botânico possui várias coleções vegetacionais.";
                answer = true;
                break;
            case 4:
                questionText.text = "O Jardim das Sensações é um espaço de destinado a interação com o meio ambiente através dos sentidos.";
                answer = true;
                break;
            case 5:
                questionText.text = "A araucária é uma árvore nativa do Paraná que pode ficar gigante!";
                answer = true;
                break;
            case 6:
                questionText.text = "É permitida a prática de esportes com bola e a utilização de bicicletas no Jardim Botânico.";
                answer = false;
                break;
        }
    }

    public void Answer(bool choice){
        if (choice == answer){
            rightAnswerText.text = "Acertou! Pode continuar! <size=72>Mas atenção, pois você só tem mais " + player.lifes + " vida(s) restante(s)!</size>";
            rightAnswer.SetActive(true);
            gameObject.SetActive(false);
        } else {
            wrongAnswer.SetActive(true);
            gameObject.SetActive(false);
        }
    }

}
