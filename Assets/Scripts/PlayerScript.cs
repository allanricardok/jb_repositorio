﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour {

	public float jumpPower = 10f;
	Rigidbody2D myRigidbody;
	private bool isGrounded = false;
	float posX = 0f;

	public GameObject Pausa_Object;
	public GameObject Pausa2_Object;
	public GameObject GameOver_Object;
	public GameObject Quiz_Object;
	public GameObject Lifeless_Object;
	public int lifes;
	
	private int rosa;
	public Text scoreText;
	public Text scoreText2;
	public Text scoreText3;
	public GameObject novo;
	public AudioClip morreu;
	public AudioClip som_rosa;
	AudioSource audioSource;

	// Use this for initialization
	void Start () {
		Pausa2_Object.SetActive (true);
		Time.timeScale = 1f;
		myRigidbody = transform.GetComponent<Rigidbody2D> ();
		posX = transform.position.x;
		rosa = PlayerPrefs.GetInt("CurrentScore", 0);
		PlayerPrefs.SetInt("CurrentScore", 0);
		novo.SetActive (false);
		audioSource = GetComponent<AudioSource> ();
		lifes = PlayerPrefs.GetInt("lifes", 2);
		PlayerPrefs.SetInt("lifes", 0);
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (transform.position.x < posX) {
			GameOver ();
		}
		
	}

	void Update() {
		if (Input.anyKeyDown && isGrounded) {
			myRigidbody.AddForce (Vector3.up * (jumpPower * myRigidbody.mass * myRigidbody.gravityScale * 20f));
		}
		scoreText.text = "Pinhões: " + rosa.ToString ();

	}



	public void Restart(string name){
		PlayerPrefs.SetInt("lifes", 2);
		PlayerPrefs.SetInt("CurrentScore", 0);
		Time.timeScale = 1f;
		SceneManager.LoadScene (name);
	}


	void OnCollisionEnter2D(Collision2D other){
	
		if (other.collider.tag == "Ground") {
			isGrounded = true;
		}
		if (other.collider.tag == "Enemy") {
			GameOver ();
			Debug.Log ("Game_Over_Inimigo");
		}


	}
	void OnCollisionStay2D(Collision2D other){

		if (other.collider.tag == "Ground") {
			isGrounded = true;	
		}
	}
	void OnCollisionExit2D(Collision2D other){

		if (other.collider.tag == "Ground") {
			isGrounded = false;
		}
	}

	void OnTriggerEnter2D(Collider2D other){
	
		if (other.tag == "Rosa") {
			audioSource.PlayOneShot (som_rosa, 1f);
			rosa++;
			scoreText.text = "Pinhões: " + rosa.ToString ();

		}

	}

	void GameOver(){
		Pausa2_Object.SetActive (false);
		audioSource.PlayOneShot (morreu, 1f);
		PlayerPrefs.SetInt ("score", rosa);
		scoreText2.text =  rosa.ToString () + " Pinhões!";

		if(rosa > PlayerPrefs.GetInt("Best")){
			PlayerPrefs.SetInt ("Best", rosa);
			novo.SetActive (true);
		}
		scoreText3.text =  "Record: " + PlayerPrefs.GetInt("Best").ToString ();
		lifes --;
		ShowQuiz();
		Time.timeScale = 0f;
		GameOver_Object.SetActive(true);
	}

	void ShowQuiz(){
		//Debug.Log("Vidas Restantes: " + lifes.ToString() + (lifes >= 0).ToString());
		if (lifes >= 0){
			//Debug.Log("Entrou");
			Quiz_Object.SetActive(true);
			Quiz_Object.GetComponent<Quiz>().Question();
		} else{
			//Debug.Log("Não Entrou");
			Quiz_Object.SetActive(false);
			Lifeless_Object.SetActive(true);
		}
	}
	
	public void Continue(){
		PlayerPrefs.SetInt("CurrentScore", rosa);
		PlayerPrefs.SetInt("lifes", lifes);
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void Pausa(){
		Time.timeScale = 0f;
		Pausa_Object.SetActive(true);
	}

	public void DesPausa(){
		Time.timeScale = 1f;
		Pausa_Object.SetActive (false);
	}

}
