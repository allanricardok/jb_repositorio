﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChallengeController : MonoBehaviour {


	public float scrollSpeed = 5f;
	public GameObject[] challenges;
	public float frequency = 1f;
	float counter = 0f;
	public Transform challengesSpawnPoint;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (counter <= 0f) {
			GenerateChallenge ();
		} else {
			counter -= Time.deltaTime * frequency;
		}


		GameObject currentChild;
		for (int i = 0; i < transform.childCount; i++) {
			currentChild = transform.GetChild(i).gameObject;
			ScrollChallenge (currentChild);
			if (currentChild.transform.position.x < -20f) {
				Destroy (currentChild);
			}
		}
	}

	void ScrollChallenge (GameObject currentChallenge){
		currentChallenge.transform.position -= Vector3.right * (scrollSpeed * Time.deltaTime);
	}

	void GenerateChallenge(){
		GameObject newChallenge = Instantiate (challenges[Random.Range(0,challenges.Length)], challengesSpawnPoint.position, Quaternion.identity) as GameObject;
		newChallenge.transform.parent = transform;
		counter = 1f;

	}
}
