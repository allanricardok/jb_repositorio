﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bg_repete : MonoBehaviour {

/* PENDENCIAS NESTE CÓDIGO:
1 - Pegar o tamanho da câmera automaticamente, pois a metade deste valor é utilizado no Update e no RpositionBackground;
2 - Adaptar os étodos para que o scroll funcione tanto no eixo X quanto o Y;
*/

	private BoxCollider2D BG_collider;
	public float BGHorizontalLenght;

	void Start () {
		BG_collider = GetComponent<BoxCollider2D> ();
		BGHorizontalLenght = GetComponent<SpriteRenderer>().size.x * transform.localScale.x;
		if (gameObject.name == "BG2 copia"){
			Debug.Log(gameObject.GetComponent<SpriteRenderer>().size);
		}
		
	}

	void Update () {
		if (transform.position.x <= -1 * (BGHorizontalLenght/2 + 2.8f)) {
			RepositionBackground ();
		}
	}

	private void RepositionBackground(){
		Vector2 BGOffset = new Vector2 (BGHorizontalLenght - 2.8f + BGHorizontalLenght/2 , transform.position.y);
		transform.position = BGOffset;
	}
}
