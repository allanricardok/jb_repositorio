﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Volume_Mute : MonoBehaviour {
    public GameObject AL;
	public GameObject Load;


                            
	void Start(){
		Load.SetActive (false);
	}

    void Update () {
        if (PlayerPrefs.GetInt("Mudo") == 1)
        {
            AudioListener.volume = 0.0F;
            PlayerPrefs.SetInt("Mudo", 2);
        }
        else if (PlayerPrefs.GetInt("Mudo") == 0)
        {
            AudioListener.volume = 1F;
        }



        
    }

    public void ON()
    {
        PlayerPrefs.SetInt("Mudo", 1);
    }
    public void OFF()
    {
        PlayerPrefs.SetInt("Mudo", 0);
    }

	public void QuitGame(){
		Application.Quit();
	}

	public void DeletePlayerprefs()
	{
		PlayerPrefs.DeleteAll();
	}

	public void Link_Sabiarts()
	{
		Application.OpenURL("https://sabiarts.itch.io/");
	}

	public void LOAD(){
	
		Load.SetActive (true);

	}


}
